<?php

  $pdo_statement = $pdo_conn->prepare("SELECT YEAR(datecreated) as tahun, SUM(CASE WHEN jenis_kelamin = 'laki-laki' THEN 1 ELSE 0 END) as lakilaki, SUM(CASE WHEN jenis_kelamin = 'perempuan' THEN 1 ELSE 0 END) as perempuan, COUNT(*) as jumlah from santri GROUP by tahun");
  $pdo_statement->execute();
  $result = $pdo_statement->fetchAll();

  $chart_data = '';

  if(!empty($result)) { 
  foreach($result as $row) {
    $chart_data .= "{ tahun:'".$row["tahun"]."', lakilaki:".$row["lakilaki"].", perempuan:".$row["perempuan"]."}, ";
    }
  }
  $chart_data = substr($chart_data, 0, -2);
  // echo $chart_data;

?>
<script src="raphael-min.js"></script>
<script src="morris.min.js"></script>
<meta charset=utf-8 />


<style type="text/css">
  .morris-hover {
  position:absolute;
  z-index:1000;
}

.morris-hover.morris-default-style {     border-radius:10px;
  padding:6px;
  color:#666;
  background:rgba(255, 255, 255, 0.8);
  border:solid 2px rgba(230, 230, 230, 0.8);
  font-family:sans-serif;
  font-size:12px;
  text-align:center;
}

.morris-hover.morris-default-style .morris-hover-row-label {
  font-weight:bold;
  margin:0.25em 0;
}

.morris-hover.morris-default-style .morris-hover-point {
  white-space:nowrap;
  margin:0.1em 0;
}

svg { width: 100%; }
</style>

<div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Bar Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>

            <div class="box-body">


                <div class="row">
      <div class="col-md-12">
        <br>
        <div id="bar-chart"></div>
      </div>
      
    </div>
  </section>
  <script type="text/javascript">
    $(document).ready(function() {
      barChart();
    $(window).resize(function() {
      window.barChart.redraw();
    
      });
    });

    function barChart() {
      window.barChart = Morris.Bar({
        element: 'bar-chart',
        data: [ <?php echo $chart_data; ?>],
        xkey: ['tahun'],
        ykeys: ['lakilaki','perempuan'],
        labels: ['Laki-laki','Perempuan'],
        lineColors: ['#1e88e5','#ff3321'],
        lineWidth: '3px',
        resize: true,
        redraw: true

      });
    }
  </script>
  </div>
            <!-- /.box-body -->
</div>
