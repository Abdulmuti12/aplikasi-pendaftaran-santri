
<?php


if(!empty($_POST["save_record"])) {
	@$pdo_statement=$pdo_conn->prepare("update indetitas_wali set nama_lengkap='" . $_POST[ 'nama_lengkap' ] . "', 
										tempat_lahir='" . $_POST[ 'tempat_lahir' ]. "',
										tanggal_lahir='" . $_POST[ 'tanggal_lahir' ]. "',
										alamat='" . $_POST[ 'alamat' ] . "',
										no='" . $_POST[ 'no' ] . "',
										rt='" . $_POST[ 'rt' ] . "',
										rw='" . $_POST[ 'rw' ] . "',
										desa='" . $_POST[ 'desa' ] . "',
										kecamatan='" . $_POST[ 'kecamatan' ] . "',
										kabupaten='" . $_POST[ 'kabupaten' ] . "',
										provinsi='" . $_POST[ 'provinsi' ] . "',
										kode_pos='" . $_POST[ 'kode_pos' ] . "',
										nik='" . $_POST[ 'nik' ] . "',
										pendidikan_terakhir='" . $_POST[ 'pendidikan_terakhir' ] . "',
										pekerjaan='" . $_POST[ 'pekerjaan' ] . "',
										hp='" . $_POST[ 'hp' ] . "',

										penghasilan_perbulan='" . $_POST[ 'penghasilan_perbulan' ] . "' where id_wali=" . $_GET["id_wali"]);

	$result = $pdo_statement->execute();
	if($result) {

		echo '<META HTTP-EQUIV="Refresh" Content="0;URL=index.php?halaman=identitas_wali ">';
	}
}
$pdo_statement = $pdo_conn->prepare("SELECT * FROM indetitas_wali where id_wali=" . $_GET["id_wali"]);
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();

?>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
            	<div class="box-header">
              		<h3 class="box-title">Edit Indentitas Wali</h3>
            	</div>
            <!-- /.box-header -->
            	<div class="box-body">
 
					 <form  action="" method="POST">

	        	<table class="table">
	           		<tr>
	           			<td><label>Nama Lengkap</label></td>
	           			<td><input type="text" name="nama_lengkap" class="form-control" value="<?php echo $result[0]['nama_lengkap']; ?>" required=""></td>
	           			<td><label>Tempat Tanggal Lahir</label></td>
	           			<td><input type="text" name="tempat_lahir" size="9" value="<?php echo $result[0]['tempat_lahir']; ?>" required> 
	           				<input type="date" size="8" name="tanggal_lahir"  value="<?php echo $result[0]['tanggal_lahir']; ?>" required=""> 
	           			 </td>
	           			
	           		</tr>	

					<tr>
	        				<td><label>Alamat Lengkap</label></td>
	        			</tr>	
	        			<tr>
	        			<td>
	        				<label class="kecil">Jl/Kp:</label>
	        				<input type="text" name="alamat" class="form-control"  value="<?php echo  $result[0]['alamat']; ?>">
	        				
	        			</td>
	        			<td>
	        				<label class="kecil">RT:</label>
	        				<input type="text" class="form-control" name="rt"  value="<?php echo  $result[0]['rt']; ?>">
	        				
	        			</td>

	        			<td>
	        				<label class="kecil">RW:</label>
	        				<input type="text" name="rw" class="form-control"  value="<?php echo  $result[0]['rw']; ?>">
	        				
	        			</td>
	        			<td>
	        				<label class="kecil">Desa</label>
	        				<input type="text" name="desa" class="form-control"  value="<?php echo  $result[0]['desa']; ?>">
	        				
	        			</td>		
	        		</tr>

	        			<tr>
	        			<td>
	        				<label class="kecil">Kecamatan</label>
	        				<input type="text" name="kecamatan" class="form-control"  value="<?php echo  $result[0]['kecamatan']; ?>">
	        				
	        			</td>
	        			<td>
	        				<label class="kecil">Kabupaten:</label>
	        				<input type="text" name="kabupaten" class="form-control"  value="<?php echo  $result[0]['kabupaten']; ?>">
	        				
	        			</td>

	        			<td>
	        				<label class="kecil">Provinsi:</label>
	        				<input type="text" name="provinsi" class="form-control"  value="<?php echo  $result[0]['provinsi']; ?>">
	        				
	        			</td>
	        			<td>
	        				<label class="kecil">Kode Pos</label>
	        				<input type="text" name="kode_pos" class="form-control"  value="<?php echo  $result[0]['kode_pos']; ?>">	        				
	        			</td>		
	        		</tr>
	
	


					<tr>
						<td><label>NIK</label></td><td><input type="text" name="nik" class="form-control" placeholder="NIK.." value="<?php echo $result[0]['nik']; ?>"></td>

	           			<td><label>Sekolah Jenjang Sebelumnya</label></td>
	           			<td>
	           				<input type="radio" name="pendidikan_terakhir" value='SD/MI' <?php if($result[0]['pendidikan_terakhir']=="SD/MI"){ echo "checked";}?> > SD/MI &nbsp; 
							<input type="radio" name="pendidikan_terakhir" value='SMP/MTS' <?php if($result[0]['pendidikan_terakhir']=="SMP/MTS"){ echo "checked";}?> > SMP/MTS &nbsp;
							<input type="radio" name="pendidikan_terakhir" value='SMA/SMK' <?php if($result[0]['pendidikan_terakhir']=="SMA/SMK"){ echo "checked";}?> > SMA/SMK &nbsp;
						</td>
					</tr>

					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td><input type="radio" name="pendidikan_terakhir" value='D3' <?php if($result[0]['pendidikan_terakhir']=="D3"){ echo "checked";}?>/> D3 &nbsp; 
							<input type="radio" name="pendidikan_terakhir" value='S1' <?php if($result[0]['pendidikan_terakhir']=="S1"){ echo "checked";}?> > S1 &nbsp;
							<input type="radio" name="pendidikan_terakhir" value='Lainnya' <?php if($result[0]['pendidikan_terakhir']=="Lainnya"){ echo "checked";}?> / > Lainnya &nbsp;
						</td>

					</tr>	

					<tr>
						<td><label>Pekerjaan</label></td>
						<td><input type="text" name="pekerjaan" class="form-control" placeholder="Pekerjaan.." value="<?php echo $result[0]['pekerjaan']; ?>">
						</td>
						<td><label>No HP/WA</label></td>
						<td><input type="text" name="hp" class="form-control" placeholder="HP.." value="<?php echo $result[0]['hp']; ?>">
						</td>

	           			
	           		</tr>	

					<tr>	
	           			<td><label>Penghasilan Perbulan</label></td>
	           			<td>
	           				
	           					<input type="radio"   name='penghasilan_perbulan' value="<= Rp.500.00" <?php if($result[0]['penghasilan_perbulan']=="<= Rp.500.00"){ echo "checked";}?> >&nbsp; 
	           				<font size="2"><= Rp.500.000 </font>
	           			</td>
	           			<td>
							<input type="radio"   name='penghasilan_perbulan' value="Rp.500.000-Rp.1000.000" <?php if($result[0]['penghasilan_perbulan']=="Rp.500.000-Rp.1000.000"){ echo "checked";}?>/>&nbsp; 
							<font size="2">Rp.500.000-Rp.1000.000</font>
							
	           			</td>
	           			<td>
							<input type="radio"   name='penghasilan_perbulan' value="Rp.1000.000-Rp.2000.000" <?php if($result[0]['penghasilan_perbulan']=="Rp.1000.000-Rp.2000.000"){ echo "checked";}?>/>&nbsp; Rp.1000.000-Rp.2000.000
							<!-- <input type="radio"   name='penghasilan_perbulan' value="Rp.2000.000-Rp.3000.000"/>&nbsp; Rp.2000.000-Rp.3000.000 -->
						</td>

					</tr>

					<tr>
						<td></td>
						
						<td>
	           				
	           					<input type="radio" class="minimal"   name='penghasilan_perbulan' value="Rp.2000.000-Rp.3000.000" <?php if($result[0]['penghasilan_perbulan']=="Rp.2000.000-Rp.3000.000"){ echo "checked";}?> >&nbsp; 
	           				<font size="2">Rp.2000.000-Rp.3000.000 </font>
	           			</td>
	           			<td>
							<input type="radio"   name='penghasilan_perbulan' value="Rp.3000.000-Rp.5000.000" <?php if($result[0]['penghasilan_perbulan']=="Rp.500.000-Rp.1000.000"){ echo "checked";}?>/>&nbsp; 
							<font size="2">Rp.3000.000-Rp.5000.000</font>
							
	           			</td>
	           			<td>
							<input type="radio"   name='penghasilan_perbulan' value="> Rp.5000.000" <?php if($result[0]['penghasilan_perbulan']=="> Rp.5000.000"){ echo "checked";}?>/>&nbsp; > Rp.5000.000
							<!-- <input type="radio"   name='penghasilan_perbulan' value="Rp.2000.000-Rp.3000.000"/>&nbsp; Rp.2000.000-Rp.3000.000 -->
						</td>
					</tr>
	           </table>
				
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<input name="save_record" type="submit" value="Save" class="btn btn-primary">

						  				</form>

				</div>	

			</div>
		</div>
	</div>
</section>