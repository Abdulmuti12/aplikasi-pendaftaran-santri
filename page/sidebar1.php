<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      
          <div class="row">
        <div class="col-xs-6 col-md-12">
        <!--jika gambar belum di upload maka mengungunkan gambar default -->          
          <?php
            if($data['foto'] != "")
            {
            ?>
              <a href="index.php?halaman=foto_admin" class="thumbnail">
                <img class="img-responsive" src="foto/<?php echo $data['foto']; ?>">
              </a>
          <?php
            }
            else{
          ?>    
              <a href="index.php?halaman=foto_admin" class="thumbnail">

              <img class="img-rounded"  src="img/images.jpg">
              </a>
          <?php
            }
          ?>
          
        </div>      
    </div>
      <!-- search form -->
     
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menu Navigasi</li>

        <?php 
          if($data['nama'] != "tamu"){
        ?>    
        <li>
          <a href="index.php?halaman=admin">
            <i class="fa fa-th"></i> <span>Admin</span>
          </a>
        </li>
        <?php
      }
      ?>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Data Santri</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.php?halaman=santri"><i class="fa fa-circle-o"></i>Indentitas Santri</a></li>
            <li><a href="index.php?halaman=identitas_ayah"><i class="fa fa-circle-o"></i>Identitas Ayah</a></li>
            <li><a href="index.php?halaman=identitas_ibu"><i class="fa fa-circle-o"></i>Identitas Ibu</a></li>
            <li><a href="index.php?halaman=identitas_wali"><i class="fa fa-circle-o"></i>Identitas Wali</a></li>
          </ul>
        </li>

        

        <li class="treeview">
          <a href="#">
            <i class="fa fa-envelope"></i> <span>Security</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.php?halaman=ganti_password"><i class="fa fa-circle-o"></i> Change Password</a></li>
            <li><a href="../examples/profile.html"><i class="fa fa-circle-o"></i>Add Security</a></li>
           
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Multilevel</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Level One
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.php?halaman=laporan_data"><i class="fa fa-circle-o"></i>Data</a></li>
            <li><a href="index.php?halaman=laporan_graph"><i class="fa fa-circle-o"></i>Graph</a></li>
            
          </ul>
        </li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

