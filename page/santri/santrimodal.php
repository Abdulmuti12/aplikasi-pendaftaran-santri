<?php

if(!empty($_POST["add_record"])) {
	$input = "INSERT INTO santri (nama_lengkap,tempat_tanggal_lahir,tanggal_lahir,jenis_kelamin,tingkat_pendidikan,status_tempat_pendidikan,
	nik_santri,anak_ke,alamat_lengkap,no,rt,rw,desa,kecamatan,kabupaten,provinsi,kode_pos,asal_sekolah,kelas,riwayat_kesahatan,golongan_darah,hobi,cita_cita,ukuran_baju,datecreated,status_orangtua) VALUES 

								(:nama_lengkap,:tempat_tanggal_lahir,:tanggal_lahir,:jenis_kelamin,:tingkat_pendidikan,:status_tempat_pendidikan,:nik_santri,:anak_ke,:alamat_lengkap,:no,:rt,:rw,:desa,:kecamatan,:kabupaten,:provinsi,:kode_pos,:asal_sekolah,:kelas,:riwayat_kesahatan,
								:golongan_darah,:hobi,:cita_cita,:ukuran_baju,:datecreated,:status_orangtua)";
							
	$a = $pdo_conn->prepare( $input );
		
	$result =$a->execute(array(':nama_lengkap'=>$_POST['nama_lengkap'],
								':tempat_tanggal_lahir'=>$_POST['tempat_tanggal_lahir'],
								':tanggal_lahir'=>$_POST['tanggal_lahir'],
								':jenis_kelamin'=>$_POST['jenis_kelamin'],
								':tingkat_pendidikan'=>$_POST['tingkat_pendidikan'],
								':status_tempat_pendidikan'=>$_POST['status_tempat_pendidikan'],
								':nik_santri'=>$_POST['nik_santri'],
								':anak_ke'=>$_POST['anak_ke'],':alamat_lengkap'=>$_POST['alamat_lengkap'],
								':no'=>$_POST['no'],
								':rt'=>$_POST['rt'],':rw'=>$_POST['rw'],':desa'=>$_POST['desa'],
								':kecamatan'=>$_POST['kecamatan'],
								':kabupaten'=>$_POST['kabupaten'],
								':provinsi'=>$_POST['provinsi'],':kode_pos'=>$_POST['kode_pos'],
								':asal_sekolah'=>$_POST['asal_sekolah'],
								':kelas'=>$_POST['kelas'],
								':riwayat_kesahatan'=>$_POST['riwayat_kesahatan'],
								':golongan_darah'=>$_POST['golongan_darah'],
								':hobi'=>$_POST['hobi'],':cita_cita'=>$_POST['cita_cita'],
								':ukuran_baju'=>$_POST['ukuran_baju'],
								':datecreated'=>$_POST['datecreated'],
								':status_orangtua'=>$_POST['status_orangtua']));

	
	if (isset($_SERVER['QUERY_STRING'])) {
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=index.php?halaman=input2">';
    }
}


?>



<div id="add_admin" class="modal fade">		
<br>	
	<div class="col-xs-1 ">
	</div>

	<div class="col-xs-10 ">
		<div class="box">
	        <div class="box-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        </div>
	        <div class="box-body">
	        <form name="frmAdd" action="" method="POST">

	        	<table class="table">
	           		<tr>
	           			<td><label>Nama Lengkap</label></td>
	           			<td><input type="text" name="nama_lengkap" class="form-control" placeholder="Nama Lengkap .." required=""></td>
	           			<td><label>Tempat Tanggal Lahir</label></td>
	           			<td><input type="text" name="tempat_tanggal_lahir" class="form-control" placeholder="Tempat Tanggal Lahir .." required="">
	           				<input type="date" name="tanggal_lahir" class="form-control" placeholder="Tempat Tanggal Lahir .." required="">
	           			</td>
	           			
	           		</tr>


					<tr>
	           			<td><label>Jenis Kelamin</label></td><td><input type="radio"  value="Laki-laki" name='jenis_kelamin' required>&nbsp; Laki-laki
							<input type="radio"  value="Perempuan" name='jenis_kelamin' required="">&nbsp; Perempuan</td>
	           			<td><label>Sekolah Jenjang Sebelumnya</label></td>
	           			<td><input type="radio" name="tingkat_pendidikan" value='SD/MI' required=""> SD/MI &nbsp; 
							<input type="radio" name="tingkat_pendidikan" value='SMP/MTS' required=""> SMP/MTS &nbsp;
							<input type="radio" name="tingkat_pendidikan" value='SMA/SMK' required=""> SMA/SMK &nbsp;
						<input type="radio" name="tingkat_pendidikan" value='D3' required=""> D3 &nbsp; 
							<input type="radio" name="tingkat_pendidikan" value='S1' required=""> S1 &nbsp;
							<input type="radio" name="tingkat_pendidikan" value='Lainnya' required=""> Lainnya &nbsp;
						</td>

					</tr>	


					<tr>
	           			<td><label>Status Tempat Pendidikan </label></td><td>
	           				<input type="radio" name="status_tempat_pendidikan" value='Negri' required=""> Negri &nbsp; 
							<input type="radio" name="status_tempat_pendidikan" value='Suwasta' required=""> Suwasta &nbsp;
	           			</td>
	           			<td><label>NIK Santri </label></td><td><input type="text" name="nik_santri" class="form-control" placeholder="NIK Santri.."></td>
					</tr>
					<tr>
	           			<td><label>Anak Ke </label></td><td><input type="text" name="anak_ke" class="form-control" placeholder="Anak Ke.."></td>
	           			
					</tr>

					<tr>
	           			<td><label>Alamat Lengkap </label></td>
	           			<td>
	           				<input type="text" name="alamat_lengkap" class="form-control" placeholder="Jln/Kp."><input type="text" name="no" class="form-control" placeholder="No"><input type="text" name="rt" placeholder="RT" class="form-control">
	           			</td>
	           		
	           			<td>
	           				<input type="text" name="rw" class="form-control" placeholder="RW"><input type="text" name="desa" class="form-control" placeholder="Desa"><input type="text" name="kecamatan" class="form-control" placeholder="Kecamatan">
	           			</td>
	           			<td>
	           				<input type="text" name="kabupaten" class="form-control" placeholder="Kabupaten">
	           				<input type="text" name="provinsi" class="form-control" placeholder="Provinsi">
	           				<input type="text" name="kode_pos" class="form-control" placeholder="kode Pos">
	           			</td>            						     					
					</tr>


					</tr>	

					<tr>
						<td><label>Asal Sekolah<label></td>
						<td><input type="text" name="asal_sekolah" class="form-control" size="16" placeholder="Asal Sekolah"></td>
						<td><label>Kelas<label></td>
						<td><input type="text" class="form-control" name="kelas" size="16" placeholder="kelas"></td>
					</tr>	


					<tr>
						<td><label>Riwayat Kesehatan</label></td>
						<td><textarea name="riwayat_kesahatan" class="form-control" placeholder="Riwayat Kesehatan .."></textarea></td>
						<td><label>Golongan Darah</label></td>
						<td><input type="radio" name="golongan_darah" value='A' required=""> A &nbsp; 
							<input type="radio" name="golongan_darah" value='B' required=""> B &nbsp;
							<input type="radio" name="golongan_darah" value='O' required=""> O &nbsp;
							<input type="radio" name="golongan_darah" value='AB' required=""> AB &nbsp;
						</td>
					</tr>	


					<tr>
						<td><label>Hobi</label></td>
						<td><textarea name="hobi" class="form-control" placeholder="Hobi .."></textarea></td>
						<td><label>Cita-Cita</label></td>
						<td><input type="text" class="form-control" name="cita_cita" size="16" placeholder="Cita-cita .."></td>
					</tr>	


					<tr>
						<td><label>Ukuran Baju</label></td>
						<td><input type="radio" name="ukuran_baju" value='S' required=""> S &nbsp; 
							<input type="radio" name="ukuran_baju" value='M' required=""> M &nbsp;
							<input type="radio" name="ukuran_baju" value='L' required=""> L &nbsp;
							<input type="radio" name="ukuran_baju" value='XL' required=""> XL &nbsp;
							<input type="hidden" name="datecreated" value='<?php echo date('Y-m-d'); ?>' required=""> 

						</td>
						<td><label>Status Ayah & Ibu </label></td>
						<td><input type="radio" name="status_orangtua" value="Masih ada" required=""> Masih Ada &nbsp; 
							<input type="radio" name="status_orangtua" value="Yatim" required=""> Yatim &nbsp;
							<input type="radio" name="status_orangtua" value="Piatu" required=""> Piatu &nbsp;
							<input type="radio" name="status_orangtua" value="Yatim Dan Piatu" required=""> Yatim Dan Piatu &nbsp;
							

						</td>
					</tr>	



	           </table>
			</div>
			<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<input type="submit" name="add_record" class="btn btn-primary" value="Simpan">
						
						</form>
				</div>
		</div>		
	</div>

	<div class="col-xs-1 ">
	</div>

</div>