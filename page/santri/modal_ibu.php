<?php

if(!empty($_POST["add_record"])) {
	$input = "INSERT INTO indetitas_ibu (nama_lengkap,tempat_lahir,tanggal_lahir,id_santri,alamat,no,rt,rw,desa,kecamatan,kabupaten,provinsi,kode_pos,nik,pendidikan_terakhir,pekerjaan,hp,penghasilan_perbulan) VALUES 
							(:nama_lengkap,:tempat_lahir,:tanggal_lahir,:id_santri,:alamat,:no,:rt,:rw,:desa,:kecamatan,:kabupaten,:provinsi,:kode_pos,:nik,:pendidikan_terakhir,:pekerjaan,:hp,:penghasilan_perbulan)";
							
	$a = $pdo_conn->prepare( $input );		
	$result =$a->execute(array(':nama_lengkap'=>$_POST['nama_lengkap'],
								':tempat_lahir'=>$_POST['tempat_lahir'],
								':tanggal_lahir'=>$_POST['tanggal_lahir'],
								':id_santri'=>$_POST['id_santri'],
								':alamat'=>$_POST['alamat'],
								':no'=>$_POST['no'],
								':rt'=>$_POST['rt'],':rw'=>$_POST['rw'],':desa'=>$_POST['desa'],
								':kecamatan'=>$_POST['kecamatan'],
								':kabupaten'=>$_POST['kabupaten'],
								':provinsi'=>$_POST['provinsi'],
								':kode_pos'=>$_POST['kode_pos'],
								':nik'=>$_POST['nik'],
								':pendidikan_terakhir'=>$_POST['pendidikan_terakhir'],
								':pekerjaan'=>$_POST['pekerjaan'],
								':hp'=>$_POST['hp'],
								':penghasilan_perbulan'=>$_POST['penghasilan_perbulan']));

	if (isset($_SERVER['QUERY_STRING'])) {
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=index.php?halaman=identitas_ibu">';
    }
}


?>

<div id="ibu" class="modal fade">		
<br>	
	<div class="col-xs-2 ">
	</div>

	<div class="col-xs-8 ">
		<div class="box">
	        <div class="box-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        </div>
	        <div class="box-body">
	        <form name="frmAdd" action="" method="POST">

	        	<table class="table">
	           		<tr>
	           			<td><label>Nama Lengkap</label></td>
	           			<td><input type="text" name="nama_lengkap" class="form-control" placeholder="Nama Lengkap .." required=""></td>
	           			<td><label>Tempat Lahir</label></td>
	           			<td><input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Tanggal Lahir .." required=""></td>
	           			
	           		</tr>


					<tr>
	           			<td><label>Tanggal Lahir</label></td>
	           			<td><input type="date" name="tanggal_lahir" class="form-control" placeholder="Tempat Tanggal Lahir .." required=""></td>
	           		
				      	<td>
	           				<input type="hidden" name="id_santri" value="<?php echo $santri['id_santri']; ?>">
				       </td>
					</tr>


					<tr>
	           			<td><label>Alamat Lengkap </label></td><td><input type="text" name="alamat" class="form-control" placeholder="Jln/Kp."><input type="text" class="form-control" name="no"  placeholder="No"></td>
	           			
	           			<td><input type="text" name="rt" class="form-control" placeholder="RT"><input type="text" class="form-control" name="rw" placeholder="RW"><input type="text" class="form-control" name="desa"  placeholder="Desa">	           						 
	           			</td>
					</tr>

					<tr>
						<td></td>
						<td><input type="text" name="kecamatan" class="form-control" placeholder="Kecamatan">
	           						 <input type="text" class="form-control" name="kabupaten"  placeholder="Kabupaten">
	           						 
	           						            						 
	           			</td>

	           			<td><input type="text" class="form-control" name="provinsi"  placeholder="Provinsi">	
						<input type="text" class="form-control" name="kode_pos" placeholder="kode Pos">
						</td>
					</tr>	

					<tr>
						<td><label>NIK<label></td>
						<td><input type="text" name="nik" class="form-control" size="16" placeholder="NIK"></td>
						<td><label>Pendidikan Terakhir<label></td>
	           			<td><input type="radio" name="pendidikan_terakhir" value='SD/MI' required=""> SD/MI &nbsp; 
							<input type="radio" name="pendidikan_terakhir" value='SMP/MTS' required=""> SMP/MTS &nbsp;
							<input type="radio" name="pendidikan_terakhir" value='SMA/SMK' required=""> SMA/SMK &nbsp;
						</td>
						


					</tr>	

					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td><input type="radio" name="pendidikan_terakhir" value='D3' required=""> D3 &nbsp; 
							<input type="radio" name="pendidikan_terakhir" value='S1' required=""> S1 &nbsp;
							<input type="radio" name="pendidikan_terakhir" value='Lainnya' required=""> Lainnya &nbsp;
						</td>
					</tr>			


					<tr>
						<td><label>Pekerjaan</label></td>
						<td><input type="text" name="pekerjaan" class="form-control" placeholder="Pekerjaan" required=""></td>

						<td><label>No.HP/WA</label></td>
	           			<td><input type="text" name="hp" class="form-control" placeholder="Tempat Tanggal Lahir .." required=""></td>	
	           			
					</tr>	

					<tr>	
	           			<td><label>Penghasilan Perbulan</label></td>
	           			<td>
	           				
	           					<input type="radio"   name='penghasilan_perbulan' value="<= Rp.500.00">&nbsp; 
	           				<font size="2"><= Rp.500.000 </font>
	           			</td>
	           			<td>
							<input type="radio"   name='penghasilan_perbulan' value="Rp.500.000-Rp.1000.000"/>&nbsp; 
							<font size="2">Rp.500.000-Rp.1000.000</font>
							
	           			</td>
	           			<td>
							<input type="radio"   name='penghasilan_perbulan' value="Rp.1000.000-Rp.2000.000"/>&nbsp; Rp.1000.000-Rp.2000.000
						</td>

					</tr>

					<tr>
						<td></td>
						
						<td>
	           				
	           					<input type="radio" class="minimal"   name='penghasilan_perbulan' value="Rp.2000.000-Rp.3000.000">&nbsp; 
	           				<font size="2">Rp.2000.000-Rp.3000.000 </font>
	           			</td>
	           			<td>
							<input type="radio"   name='penghasilan_perbulan' value="Rp.3000.000-Rp.5000.000"/>&nbsp; 
							<font size="2">Rp.3000.000-Rp.5000.000</font>
							
	           			</td>
	           			<td>
							<input type="radio"   name='penghasilan_perbulan' value="Rp.1000.000-Rp.2000.000"/>&nbsp; > Rp.5000.000
						</td>
					</tr>	


	           </table>

			</div>

			<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<input type="submit" name="add_record" class="btn btn-primary" value="Simpan">
						
						</form>
				</div>
		</div>		
	</div>

	<div class="col-xs-2 ">
	</div>

</div>


