
<?php


if(!empty($_POST["save_record"])) {
	@$pdo_statement=$pdo_conn->prepare("update santri set nama_lengkap='" . $_POST[ 'nama_lengkap' ] . "', 
										tempat_tanggal_lahir='" . $_POST[ 'tempat_tanggal_lahir' ]. "',
										tanggal_lahir='" . $_POST[ 'tanggal_lahir' ]. "',
										jenis_kelamin='" . $_POST[ 'jenis_kelamin' ]. "',
										tingkat_pendidikan='" . $_POST[ 'tingkat_pendidikan' ] . "',
										status_tempat_pendidikan='" . $_POST[ 'status_tempat_pendidikan' ] . "',
										nik_santri='" . $_POST[ 'nik_santri' ] . "',
										anak_ke='" . $_POST[ 'anak_ke' ] . "',
										alamat_lengkap='" . $_POST[ 'alamat_lengkap' ] . "',
										no='" . $_POST[ 'no' ] . "',
										rt='" . $_POST[ 'rt' ] . "',
										rw='" . $_POST[ 'rw' ] . "',
										desa='" . $_POST[ 'desa' ] . "',
										kecamatan='" . $_POST[ 'kecamatan' ] . "',
										kabupaten='" . $_POST[ 'kabupaten' ] . "',
										provinsi='" . $_POST[ 'provinsi' ] . "',
										kode_pos='" . $_POST[ 'kode_pos' ] . "',
										asal_sekolah='" . $_POST[ 'asal_sekolah' ] . "',
										kelas='" . $_POST[ 'kelas' ] . "',
										riwayat_kesahatan='" . $_POST[ 'riwayat_kesahatan' ] . "',
										golongan_darah='" . $_POST[ 'golongan_darah' ] . "',
										ukuran_baju='" . $_POST[ 'ukuran_baju' ] . "',
										status_orangtua='" . $_POST[ 'status_orangtua' ] . "' where id_santri=" . $_GET["id_santri"]);




	$result = $pdo_statement->execute();
	if($result) {


		echo '<META HTTP-EQUIV="Refresh" Content="0;URL=index.php?halaman=santri ">';

	}
}
$pdo_statement = $pdo_conn->prepare("SELECT * FROM santri where id_santri=" . $_GET["id_santri"]);
$pdo_statement->execute();
$result = $pdo_statement->fetchAll();

?>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
            	<div class="box-header">
              		<h3 class="box-title">Edit Santri</h3>
            	</div>
            <!-- /.box-header -->
            	<div class="box-body">
 
					 <form  action="" method="POST">

	        	<table class="table">
	           		<tr>
	           			<td><label>Nama Lengkap</label></td>
	           			<td><input type="text" name="nama_lengkap" class="form-control" value="<?php echo $result[0]['nama_lengkap']; ?>" required=""></td>
	           			<td><label>Tempat Tanggal Lahir</label></td>
	           			<td>
	           				<input type="text" name="tempat_tanggal_lahir" size="7" value="<?php echo $result[0]['tempat_tanggal_lahir']; ?>" required="">
	           				<input type="date" size="10" name="tanggal_lahir" class="form-control" value="<?php echo $result[0]['tanggal_lahir']; ?>" required="">
	           			</td>
	           			
	           		</tr>


					<tr>
	           			<td><label>Jenis Kelamin</label></td><td>
	           				<input type="radio"  value="Laki-laki" name='jenis_kelamin' <?php if($result[0]['jenis_kelamin']=="Laki-laki"){ echo "checked";}?>>&nbsp; Laki-laki
							<input type="radio"  value="Perempuan" name='jenis_kelamin' <?php if($result[0]['jenis_kelamin']=="Perempuan"){ echo "checked";}?>/>&nbsp; Perempuan</td>
	           			<td><label>Sekolah Jenjang Sebelumnya</label></td>
	           			<td>
	           				<input type="radio" name="tingkat_pendidikan" value='SD/MI' <?php if($result[0]['tingkat_pendidikan']=="SD/MI"){ echo "checked";}?> > SD/MI &nbsp; 
							<input type="radio" name="tingkat_pendidikan" value='SMP/MTS' <?php if($result[0]['tingkat_pendidikan']=="SMP/MTS"){ echo "checked";}?> > SMP/MTS &nbsp;
							<input type="radio" name="tingkat_pendidikan" value='SMA/SMK' <?php if($result[0]['tingkat_pendidikan']=="SMA/SMK"){ echo "checked";}?> > SMA/SMK &nbsp;
						</td>
					</tr>

					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td><input type="radio" name="tingkat_pendidikan" value='D3' <?php if($result[0]['tingkat_pendidikan']=="D3"){ echo "checked";}?>/> D3 &nbsp; 
							<input type="radio" name="tingkat_pendidikan" value='S1' <?php if($result[0]['tingkat_pendidikan']=="S1"){ echo "checked";}?> > S1 &nbsp;
							<input type="radio" name="tingkat_pendidikan" value='Lainnya' <?php if($result[0]['tingkat_pendidikan']=="Lainnya"){ echo "checked";}?> / > Lainnya &nbsp;
						</td>

					</tr>	


					<tr>
	           			<td><label>Status Tempat Pendidikan </label></td>
	           			<td>
	           				<input type="radio" name="status_tempat_pendidikan" value='Negri' <?php if($result[0]['status_tempat_pendidikan']=="Negri"){ echo "checked";}?> /> Negri &nbsp; 
							<input type="radio" name="status_tempat_pendidikan" value='Suasta' <?php if($result[0]['status_tempat_pendidikan']=="Suwasta"){ echo "checked";}?>/ > Suwasta &nbsp;
						</td>
	           			<td><label>NIK Santri </label></td><td><input type="text" name="nik_santri" class="form-control" placeholder="NIK Santri.." value="<?php echo $result[0]['nik_santri']; ?>"></td>
					</tr>
					<tr>
	           			<td><label>Anak Ke </label></td><td><input type="text" name="anak_ke" value="<?php echo $result[0]['anak_ke']; ?>" class="form-control" value=""></td>
	           			
					</tr>
<tr>
	        			<td><label>Alamat Lengkap</label></td>
	        			</tr>	
	        			<tr>
	        			<td>
	        				<label class="kecil">Jl/Kp:</label>
	        				<input type="text" name="alamat_lengkap" class="form-control"  value="<?php echo  $result[0]['alamat_lengkap']; ?>">
	        				
	        			</td>
	        			<td>
	        				<label class="kecil">RT:</label>
	        				<input type="text" class="form-control" name="rt"  value="<?php echo  $result[0]['rt']; ?>">
	        				
	        			</td>

	        			<td>
	        				<label class="kecil">RW:</label>
	        				<input type="text" name="rw" class="form-control"  value="<?php echo  $result[0]['rw']; ?>">
	        				
	        			</td>
	        			<td>
	        				<label class="kecil">Desa</label>
	        				<input type="text" name="desa" class="form-control"  value="<?php echo  $result[0]['desa']; ?>">
	        				
	        			</td>		
	        		</tr>

	        			<tr>
	        			<td>
	        				<label class="kecil">Kecamatan</label>
	        				<input type="text" name="kecamatan" class="form-control"  value="<?php echo  $result[0]['kecamatan']; ?>">
	        				
	        			</td>
	        			<td>
	        				<label class="kecil">Kabupaten:</label>
	        				<input type="text" name="kabupaten" class="form-control"  value="<?php echo  $result[0]['kabupaten']; ?>">
	        				
	        			</td>

	        			<td>
	        				<label class="kecil">Provinsi:</label>
	        				<input type="text" name="provinsi" class="form-control"  value="<?php echo  $result[0]['provinsi']; ?>">
	        				
	        			</td>
	        			<td>
	        				<label class="kecil">Kode Pos</label>
	        				<input type="text" name="kode_pos" class="form-control"  value="<?php echo  $result[0]['kode_pos']; ?>">	        				
	        			</td>		
	        		</tr>


					<tr>
						<td><label>Asal Sekolah<label></td>
						<td><input type="text" name="asal_sekolah" value="<?php echo $result[0]['asal_sekolah']; ?>" class="form-control" size="16" placeholder="Asal Sekolah"></td>
						<td><label>Kelas<label></td>
						<td><input type="text" class="form-control" name="kelas" value="<?php echo $result[0]['kelas']; ?>" size="16" placeholder="kelas"></td>
					</tr>	


					<tr>
						<td><label>Riwayat Kesehatan</label></td>
						<td>
							<textarea name="riwayat_kesahatan" class="form-control" placeholder="Riwayat Kesehatan .."><?php echo $result[0]['riwayat_kesahatan']; ?></textarea>
						</td>
						<td><label>Golongan Darah</label></td>
						<td><input type="radio" name="golongan_darah" <?php if($result[0]['golongan_darah']=="A"){ echo "checked";}?> value='A' required=""> A &nbsp; 
							<input type="radio" name="golongan_darah" <?php if($result[0]['golongan_darah']=="B"){ echo "checked";}?> value='B' required=""> B &nbsp;
							<input type="radio" name="golongan_darah" <?php if($result[0]['golongan_darah']=="O"){ echo "checked";}?> value='O' required=""> O &nbsp;
							<input type="radio" name="golongan_darah" <?php if($result[0]['golongan_darah']=="AB"){ echo "checked";}?> value='AB' > AB &nbsp;
						</td>
					</tr>	


					<tr>
						<td><label>Hobi</label></td>
						<td><textarea name="hobi"  class="form-control" placeholder="Hobi ..">
							<?php echo $result[0]['hobi']; ?>
						</textarea></td>
						<td><label>Cita-Cita</label></td>
						<td><input type="text" name="cita_cita" value="<?php echo $result[0]['cita_cita']; ?>" class="form-control"   size="16" placeholder="Cita-cita .."></td>
					</tr>	


					<tr>
						<td><label>Ukuran Baju</label></td>
						<td><input type="radio" name="ukuran_baju" <?php if($result[0]['ukuran_baju']=="S"){ echo "checked";}?> value='S' required=""> S &nbsp; 
							<input type="radio" name="ukuran_baju" <?php if($result[0]['ukuran_baju']=="M"){ echo "checked";}?> value='M' required=""> M &nbsp;
							<input type="radio" name="ukuran_baju" <?php if($result[0]['ukuran_baju']=="L"){ echo "checked";}?> value='L' required=""> L &nbsp;
							<input type="radio" name="ukuran_baju" <?php if($result[0]['ukuran_baju']=="XL"){ echo "checked";}?> value='XL' required=""> XL &nbsp;
						</td>
				
						<td><label>Status Ayah&Ibu</label></td>
						<td><input type="radio" name="status_orangtua" <?php if($result[0]['status_orangtua']=="Masih ada"){ echo "checked";}?> value='Masih ada' required=""> Masih ada &nbsp; 
							<input type="radio" name="status_orangtua" <?php if($result[0]['status_orangtua']=="Yatim"){ echo "checked";}?> value='Yatim' required=""> Yatim &nbsp;
							<input type="radio" name="status_orangtua" <?php if($result[0]['status_orangtua']=="Piatu"){ echo "checked";}?> value='Piatu' required=""> Piatu &nbsp;
							<input type="radio" name="status_orangtua" <?php if($result[0]['status_orangtua']=="Yatim Dan Piatu"){ echo "checked";}?> value='Yatim Dan Piatu' required=""> Yatim Dan Piatu &nbsp;
						</td>
						
					</tr>	



	           </table>
				
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<input name="save_record" type="submit" value="Save" class="btn btn-primary">

						  				</form>

				</div>	

			</div>
		</div>
	</div>
</section>