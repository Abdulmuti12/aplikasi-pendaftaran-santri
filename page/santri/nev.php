<?php

if(!empty($_POST["add_record"])) {
	$input = "INSERT INTO user (nama_lengkap,tempat_tanggal_lahir,jenis_kelamin,tingkat_pendidikan,status_tempat_pendidikan,
								nik_santri,anak_ke,alamat_lengkap,no,rt,rw,desa,kecamatan,kabupaten,provinsi,kode_pos,asal_sekolah,kelas,riwayat_kesahatan,golongan_darah,hobi,cita_cita,ukuran_baju) VALUES 

								(:nama_lengkap,:tempat_tanggal_lahir,:jenis_kelamin,:tingkat_pendidikan,:status_tempat_pendidikan,
								:nik_santri,:anak_ke,:alamat_lengkap,:no,:rt,:rw,:desa,:kecamatan,:kabupaten,:provinsi,:kode_pos,:asal_sekolah,:kelas,:riwayat_kesahatan,:golongan_darah,:hobi,:cita_cita,:ukuran_baju)";

							
	$a = $pdo_conn->prepare( $input );
		
	$result =$a->execute( array(':nama_lengkap'=>$_POST['nama_lengkap'],
								':tempat_tanggal_lahir'=>$_POST['tempat_tanggal_lahir'],':jenis_kelamin'=>$_POST['jenis_kelamin'],
								':tingkat_pendidikan'=>$_POST['tingkat_pendidikan'],
								':status_tempat_pendidikan'=>$_POST['status_tempat_pendidikan'],
								':nik_santri'=>$_POST['nik_santri'] ,':anak_ke'=>$_POST['anak_ke'], 
								':alamat_lengkap'=>$_POST['alamat_lengkap'],
								':no'=>$_POST['no'], ':rt'=>$_POST['rt'], ':rw'=>$_POST['rw'], ':desa'=>$_POST['desa'], 
								':kecamatan'=>$_POST['kecamatan'], ':kabupaten'=>$_POST['kabupaten'],
								':provinsi'=>$_POST['provinsi'], ':asal_sekolah'=>$_POST['asal_sekolah'],
								':kelas'=>$_POST['kelas'], ':riwayat_kesahatan'=>$_POST['riwayat_kesahatan'],
								':golongan_darah'=>$_POST['golongan_darah'],':hobi'=>$_POST['hobi'],
								':cita_cita'=>$_POST['cita_cita'],':ukuran_baju'=>$_POST['ukuran_baju']));

	if (isset($_SERVER['QUERY_STRING'])) {
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=index.php?halaman=admin">';
    }
}

?>


	
	

<div id="add_admin" class="modal fade">		
<br>	
	<div class="col-xs-2 ">
	</div>

	<div class="col-xs-8 ">
		<div class="box">
	        <div class="box-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        </div>
	        <div class="box-body">
	        <form name="frmAdd" action="" method="POST">

	        	<table class="table">
	           		<tr>
	           			<td><label>Nama Lengkap</label></td>
	           			<td><input type="text" name="nama_lengkap" class="form-control" placeholder="Nama Lengkap .."></td>
	           			<td><label>Asal Sekolah</label></td><td><input type="text" name="asal_sekolah" class="form-control" placeholder="Asal Sekolah.."></td>
	           		</tr>
	           		<tr>
	           			<td><label>Tempat Tanggal Lahir</label></td>
	           			<td><input type="text" name="tempat_tanggal_lahir" class="form-control" placeholder="Tempat Tanggal Lahir .."></td>
	           			<td><label>Riwayat Kesehatan</label></td><td><input type="text" name="riwayat_kesahatan" class="form-control" placeholder="Riwayat Kesahatan"></td>
					</tr>

					<tr>
	           			<td><label>Jenis Kelamin</label></td><td><input type="radio"  value="Laki-laki" name='jenis_kelamin'>&nbsp; Laki-laki
							<input type="radio"  value="Perempuan" name='jenis_kelamin' >&nbsp; Perempuan</td>
	           			<td><label>Golongan Darah</label></td><td><input type="text" name="golongan_darah" class="form-control" placeholder="Golongan Darah .."></td>
					</tr>

					<tr>
	           			<td><label>Sekolah Jenjang Sebelumnya</label></td><td><input type="text" name="tingkat_pendidikan" class="form-control" placeholder="Sekolah Jenjang Sebelumnya .."></td>
	           			<td><label>Hobi</label></td><td><input type="text" name="hobi" class="form-control" placeholder="Hobi"></td>
					</tr>

					<tr>
	           			<td><label>Status Sekolah Sebelumnya</label></td><td>
	           				<input type="radio"  value="Negri" name='status_tempat_pendidikan'>&nbsp; Swasta
							<input type="radio"  value="Swasta" name='status_tempat_pendidikan' >&nbsp; Negri</td></td>
	           			<td><label>Cita-Cita</label></td><td><input type="text" name="cita_cita" class="form-control"  placeholder="Cita-Cita .."></td>
					</tr>

					<tr>
	           			<td><label>NIK Santri</label></td><td><input type="text" name="nik_santri" class="form-control" placeholder="NIK Santri .."></td>
	           			<td><label>Ukuran Baju</label></td><td><input type="text" name="ukuran_baju" class="form-control" placeholder="Ukuran Baju"></td>
					</tr>

					<tr>
	           			<td><label>Anak Ke</label></td><td><input type="text" name="anak_ke" class="form-control" placeholder="Anak Ke .."></td>
					</tr>

					<tr>
	           			<td><label>Alamat Lengkap</label></td>
	           			<td><input type="text" size="20" name="alamat_lengkap"  placeholder="Jl/Kp.">
	           				<input type="text" size="2"  name="rt"  placeholder="Rt.">
	           				<input type="text" size="2"  name="rw"  placeholder="Rw.">
	           				<input type="text" size="6"  name="kode_pos"  placeholder="KODE POS">

	           			</td>
					</tr>	

					<tr>
					<td></td>	
	           			<td><input type="text" size="10" name="kecamatan"  placeholder="Kecamatan">
	           				<input type="text" size="10"  name="kabupaten"  placeholder="Kabupaten">
	           				<input type="text" size="10"  name="provinsi"  placeholder="Provinsi">
	           			</td>
					</tr>
	           </table>
			</div>
			<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<input type="submit" name="add_record" class="btn btn-primary" value="Simpan">
						
						</form>
				</div>
		</div>		
	</div>

<div class="col-xs-2 ">
</div>

	
	</div>